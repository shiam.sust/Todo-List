<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark" id="nv">
        <a class="navbar-brand" href="/">TODO LIST</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <a class="nav-link {{ Request::is('/')? 'active' : '' }}" href="/">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link {{ Request::is('todo/create')? 'active' : '' }}" href="todo/create">Create Todo</a>
            </li>
            <li class="nav-item">
              <a class="nav-link"  href="#">About</a>
            </li>
          </ul>
          @if(!Auth::user())
            <ul class="navbar-nav ml-auto">
              <li class="nav-item">
                <a class="nav-link {{ Request::is('/login')? 'active' : '' }}" href="/login">Login <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item">
                <a class="nav-link {{ Request::is('/register')? 'active' : '' }}" href="/register">Register</a>
              </li>
            </ul>
          @else
            <ul class="navbar-nav ml-auto">
              <li class="nav-item">
                <a class="nav-link {{ Request::is('/logout')? 'active' : '' }}" href="/logout">Logout <span class="sr-only">(current)</span></a>
              </li>
            </ul>
          @endif

        </div>
      </nav>