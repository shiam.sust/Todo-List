<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Auth::routes();
Route::group(['middleware' => 'auth'], function(){
	Route::get('/', 'TodosController@index');

	/*Route::post('newtodo', [
		'uses' => 'TodosController@store'
	])->name('post.newtodo');*/

	Route::post('todo-update/{id}',[
		'uses' => 'TodosController@update'
	])->name('todo-update');

	Route::get('delete-todo/{id}',[
		'uses' => 'TodosController@destroy'
	])->name('delete-todo');

	Route::resource('todo','TodosController');

	Route::get('/home', 'TodosController@index')->name('home');

	//Route::get('logout', 'Auth\LoginController@logout');
	Route::get('logout', function(){
		Auth::logout();
		return redirect()->route('login');
	});
});

