<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Todo list</title>
        <link rel="stylesheet" type="text/css" href="/css/app.css">
    </head>
    <body>
        @include('inc.navbar')
        <div class="container" style="margin-top: 70px">
            @include('inc.messages')
            @yield('content')
        </div>

        <footer id="footer" class="text-center">
            <p>copyright &copy; 2018 Todolist</p>   
        </footer>
    </body>
</html>
