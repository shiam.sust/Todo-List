@extends('layouts.app')

@section('content')
	
	<div class="container-fluid" style="margin-top: 80px">
		<h3>Create Todo</h3>
		<form action="{{ route('todo-update',['id' => $mytodo->id]) }}" method="post">
			{{ csrf_field() }}
		  <div class="form-group">
		    <label for="title">Title</label>
		    <input type="text" class="form-control" id="title" placeholder="enter title" name="title" value="{{ $mytodo->text }}">
		  </div>
		  <div class="form-group">
			<label for="body">Details</label>
			<textarea class="form-control" rows="7" id="body" name="body">{{ $mytodo->body }}</textarea>
		  </div>
		  <div class="form-group">
		    <label for="due">Time</label>
		    <input type="text" class="form-control" id="due" placeholder="when to do" name="due" value="{{ $mytodo->due }}">
		  </div>
		  
		  <button type="submit" class="btn btn-primary">Submit</button>
		</form>
	</div>
@endsection