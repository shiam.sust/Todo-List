@extends('layouts.app')
@section('content')
    <div class="card text-center">
      <div class="card-header">
        ToDo Details
      </div>
      <div class="card-body">
        <h5 class="card-title">{{ $todo->text }}</h5>
        <p class="card-text">{{ $todo->body }}</p>
        <p class="card-text">{{ $todo->due }}</p>
      </div>
      <div class="card-footer text-muted">
        <a href="/" class="btn btn-primary">Back</a>
      </div>
    </div>
@endsection
