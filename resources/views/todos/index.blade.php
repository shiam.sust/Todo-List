@extends('layouts.app')
@section('content')
	<h1>Todos</h1>
	@if(count($todos) > 0)
		@foreach($todos as $todo)
			<div class="card">
			  <div class="card-header">
			    My ToDo List
			  </div>
			  <div class="card-body">
			    <h5 class="card-title">{{ $todo->text }}</h5>
			    <p class="card-text">{{ $todo->due }}</p>
			    <a href="todo/{{ $todo->id }}" class="btn btn-info">View Details
			    	<a href="delete-todo/{{ $todo->id }}" class="btn btn-danger float-right">Delete</a>
			    	<a href="todo/{{ $todo->id }}/edit" class="btn btn-primary float-right">Edit Information</a>
			    </a>
			  </div>
			</div>
		@endforeach
	@endif
@endsection
